<?php

$host       = "localhost";
$usuario   = "root";
$contraseña   = "admin";
$base_datos     = "database";
$dsn        = "mysql:host=$host;port=3307;dbname=$base_datos";
//$opciones    = array(
//                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
//              );
try{
    $conexion = new PDO($dsn, $usuario, $contraseña);
    $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Se ha establecido una conexión con el servidor de bases de datos";
}
catch(PDOException $e){
    echo "No se ha podido establecer conexión con el servidor de bases de datos.".$e->getMessage();
}

$conexion = null;

?>


